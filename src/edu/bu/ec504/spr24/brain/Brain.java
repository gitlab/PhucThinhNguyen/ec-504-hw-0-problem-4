package edu.bu.ec504.spr24.brain;

import edu.bu.ec504.spr24.sameGameTris.GUI;
import edu.bu.ec504.spr24.sameGameTris.SameGameTris;

/**
 * The Brain is the artificial intelligence that tries to come up with the
 * best possible moves for the current position.
 * <p>
 * It typically runs in its own thread so that it will not interfere with other processing.
 */
public abstract class Brain implements Runnable {

	// CONSTANTS

	// FIELDS
	protected final GUI myGUI; // stores the GUI class attached to this Brain
	/**
	 * When set to true, the Brain should stop what it's doing and exit as soon as is appropriate.
	 */
	private volatile boolean allDone = false;

	// METHODS

	/**
	 * Constructs the brain
	 */
	Brain() {this.myGUI = SameGameTris.getInstance();}

	/**
	 * This is called when the Brain is being asked to close down (i.e., the game is over).
	 * It should clean up any data structures and/or signal threads to close, etc.
	 */
	final public void allDone() {allDone = true;}

	/**
	 * Asks the Brain to for a move, and effects it on the GUI.
	 *
	 * @see java.lang.Runnable#run()
	 */
	final public void run() {
		if (!allDone && !myGUI.gameOverQ()) {
			Board.Pos theNextMove = nextMove();
			myGUI.makeMove(theNextMove.xx, theNextMove.yy);
		}
	}

	// ... METHODS FOR SUBCLASSES TO DEFINE

	/**
	 * Each Brain should have a name, which is provided by this method.
	 *
	 * @return the name of the brain
	 */
	public abstract String myName();

	/**
	 * Returns the next move of the Brain.
	 */
	abstract Board.Pos nextMove();

}
